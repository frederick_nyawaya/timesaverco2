package co.timesaver.timesaverco;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;


public class MainActivity extends AppCompatActivity {
    private EditText usernameEdit, passwordEdit;
    private String userNameString, passwordString;

    DatabaseHelper helper = new DatabaseHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        passwordEdit = (EditText) findViewById(R.id.TFpassword);
        usernameEdit = (EditText) findViewById(R.id.TFusername);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public void onButtonClick(View v) {
        if (v.getId() == R.id.Blogin) {
//            EditText a = (EditText) findViewById(R.id.TFusername);
//            String str = a.getText().toString();
//            EditText b = (EditText) findViewById(R.id.TFpassword);
//            String pass = b.getText().toString();
//
//            String password = helper.searchPassword(str);
//            if (password.equals(password)) {
//                Intent i = new Intent(MainActivity.this, Display.class);
//                i.putExtra("Username", str);
//                startActivity(i);
//            } else {
//                Toast temp = Toast.makeText(MainActivity.this, "Username and password don't match!", Toast.LENGTH_SHORT);
//                temp.show();
//            }

            doLogin();

        }
    }

    private void doLogin(){
        userNameString = usernameEdit.getText().toString().trim();
        if(userNameString.length() ==0){
            usernameEdit.setError("Required");
            return;
        }
        passwordString = passwordEdit.getText().toString();
        if(passwordString.length()==0){
            passwordEdit.setError("Required");
            return;
        }

        new LoginTask().execute(new String[]{userNameString, passwordString});

    }

    private class LoginTask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                return CloudWork.sendPost("intent=login&username=" + params[0] + "&password=" + params[1], CloudWork.LOGIN_URL);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            Log.i("ME", s);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
