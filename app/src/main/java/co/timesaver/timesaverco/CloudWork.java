package co.timesaver.timesaverco;

import android.util.Log;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Base class to handle calls to the server
 * Created by Frederick on 8/14/2015.
 */
public class CloudWork {
    public static final String TAG = CloudWork.class.getSimpleName();
    public static final String USER_AGENT = "";
    //base url to the php scripts ???
    public static final String BASE_URL = "http://www.timesaver.co/app-backend/";
    public static final String LOGIN_URL ="backend.php?";
    public static final String UPLOAD_DATA = "/uploads";





    public static String sendPost(String data, String url) throws Exception {
        URL obj = new URL(BASE_URL + url);

        Log.i(TAG, "request.." + obj.getPath() + "\n" + data);

        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // add reuqest header
        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(data);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();

        BufferedReader in = new BufferedReader(new InputStreamReader(
                con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        // print result
        Log.i("Moth", "responseCode - " + responseCode);

        Log.i("Moth", "response - " + response);
        if (responseCode == 200)
            return response.toString();
        else
            return null;

    }
}
