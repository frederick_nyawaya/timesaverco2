<?php


$host = "localhost";

$dbname = "gpstimesaverdb";

$options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8');

//initial query, fetches all chats for patient
if (!empty($_POST)) {
        $intent = $_POST['intent'];

        $username = $_POST['username'];
        $password = $_POST['password';]

        try {
    
            $db = new PDO("mysql:host={$host};dbname={$dbname};charset=utf8", $username, $password, $options);
        }

        catch(PDOException $ex) {
    
            die("Failed to connect to the database: " . $ex->getMessage());
        }

        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

        if (function_exists('get_magic_quotes_gpc') && get_magic_quotes_gpc()) {
    
    function undo_magic_quotes_gpc(&$array) {
        
        foreach ($array as & $value) {
            
            if (is_array($value)) {
                
                undo_magic_quotes_gpc($value);
            } 
            else {
                
                $value = stripslashes($value);
            }
        }
    }
    
    undo_magic_quotes_gpc($_POST);
    
    undo_magic_quotes_gpc($_GET);
    
    undo_magic_quotes_gpc($_COOKIE);


    header('Content-Type: text/html; charset=utf-8');

    session_start();

    if($intent == "login"){
        $response["success"] = true;
        echo json_encode($response);
    }
    else{
        //get gps data from post, insert in db
        $data = json_decode($_POST['data']);
    
        foreach ($data as $item) {
            $query = "
                INSERT INTO 
                        gps_data (username, time, latt, longi)
                    VALUES
                        (:username, :time, :latt, :longi)
                ";
            $query_params = array(':username' => $item->username,
            ':time' => $item->time, ':latt' => $item->latt, ':longi' => $item->longi);
        
            try {
            
                $stmt = $db->prepare($query);
                $result = $stmt->execute($query_params);
            }
            catch(PDOException $ex) {
                $response["success"] = 0;
                $response["message"] = "Database Error. Couldn't add gps data";
                $response["exception"] = $ex->getMessage();
            
                die(json_encode($response));
            }
            $response["success"] = 1;
            $response["message"] = "gps data Successfully Added";
    
            echo json_encode($response);
        }
    

    }

}
else{

    die("empty post request");
}


?>
